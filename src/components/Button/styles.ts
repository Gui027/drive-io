import styled from 'styled-components';

export const Container = styled.button`
  background-color: #17A4D0;
  border: 1px solid #17a4d0;
  color: #FFF;
  font-size: 14px;
  font-weight: 400;
  width: 230px;
  height: 60px;
  border-radius: 100px;
  margin-top: 30px;
  transition: all 0.25s ease-out;

  &:hover {
    background-color: #FFF;
  color: #17A4D0;
  }

  @media (max-width: 600px) {
    font-size: 11px;
    width: 130px;
    height: 50px;
  }
  `;